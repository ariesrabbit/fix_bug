import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { handleBookingChair } from "../../../redux/Action/BookingAction";
import style from "./bookingScreen.module.css";

export default function BookingScreen({ chairList }) {
  let dispatch = useDispatch();
  let listBookingChair = useSelector(
    (state) => state.BookingReducer.listBookingChair
  );

  let renderGhe = () => {
    return chairList.slice(0, 100).map((chair) => {
      let chairStyle = "";
      let disable = false;
      // ghế thường
      if (chair.loaiGhe === "Thuong") {
        chairStyle = `${style["normal_chair"]} `;
      } else {
        chairStyle = `${style["special_chair"]}`;
      }
      // ghế đã đặt
      if (chair.daDat) {
        chairStyle = `${style["ordered_chair"]}`;
        disable = true;
      }
      // ghế đang đặt
      let index = listBookingChair.findIndex(
        (bookingChair) => bookingChair.maGhe === chair.maGhe
      );
      if (index !== -1) {
        chairStyle = `${style["ordering_chair"]}`;
      }
      return (
        <button
          disabled={disable}
          onClick={() => {
            dispatch(handleBookingChair(chair));
          }}
          key={chair.maGhe}
          className={`${chairStyle} m-2`}
        >
          {chair.tenGhe}
        </button>
      );
    });
  };
  return (
    <div className="w-2/3">
      <div
        className={`w-4/5 mx-auto mt-3 ${style["screen"]} bg-black shadow-2xl flex items-end justify-center`}
      >
        <span className="text-white">Màn hình</span>
      </div>

      <div className="w-4/5 mx-auto mt-14">{renderGhe()}</div>
      <hr className="my-4 mx-auto w-4/5 h-1 bg-gray-100 rounded border-0 md:my-10 dark:bg-gray-700" />
      <div className="w-4/5 mx-auto mt-3 flex justify-center items-center font-bold space-x-2">
        <button className={`${style["normal"]}`}>x</button>
        <span>Ghế thường</span>
        <button className={`${style["special"]}`}>x</button>
        <span>Ghế Vip</span>
        <button className={`${style["ordering"]}`}>x</button>
        <span>Ghế đang đặt</span>
        <button className={`${style["ordered_chair"]}`}>x</button>
        <span>Ghế đã đặt</span>
      </div>
    </div>
  );
}
