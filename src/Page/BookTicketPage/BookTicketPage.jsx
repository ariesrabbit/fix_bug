import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieSerVice } from "../../Service/MovieService";
import BookingInfo from "./BookingTicket/BookingInfo";
import BookingScreen from "./BookingScreen/BookingScreen";
import style from "./BookingScreen/bookingScreen.module.css";

export default function BookTicketPage() {
  let [chairList, setChairList] = useState([]);
  let [infoMovie, setInfoMovie] = useState({});
  let { id } = useParams();
  let fetchApi = async ({ id }) => {
    let params = {
      MaLichChieu: id,
    };
    try {
      let res = await movieSerVice.getListTicketRoom(params);
      setChairList(res.data.content.danhSachGhe);
      setInfoMovie(res.data.content.thongTinPhim);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    fetchApi({ id });
  }, []);
  return (
    <div className={`${style["background"]}`}>
      <div className="flex items-center w-full">
        <BookingScreen chairList={chairList} />
        <BookingInfo infoMovie={infoMovie} />
      </div>
    </div>
  );
}
