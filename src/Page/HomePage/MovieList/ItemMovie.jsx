import React from "react";
import ItemMovieVideo from "./ItemMovieVideo";
import { ShoppingCartOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
export default function ItemMovie({ dataMovie }) {
  let navigate = useNavigate();
  return (
    <div className="group relative h-[26rem] max-w-sm rounded overflow-hidden shadow-lg  ">
      <div
        style={{ background: "rgba(0, 0, 0, 0.3)", transition: "0.5s" }}
        className="w-full h-full absolute top-0 left-0  group-hover:opacity-100  opacity-0 "
      >
        <div className=" flex items-center justify-center h-3/4 w-full">
          <ItemMovieVideo trailer={dataMovie.trailer} />
        </div>
        <div
          style={{ background: "rgba(0, 0, 0, 0.7)" }}
          className="h-1/4 p-1 text-center text-white  font-bold "
        >
          <p className="text-xl  h-1/2">
            {dataMovie.tenPhim.length > 17
              ? `${dataMovie.tenPhim.slice(0, 17)}...`
              : dataMovie.tenPhim}
          </p>
          <div className="flex items-center h-1/2  space-x-1 p-1 text-md ">
            <button
              onClick={() => {
                navigate(`/detail/${dataMovie.maPhim}`);
              }}
              className="h-[90%] hover:h-full w-1/2 bg-purple-700  rounded-lg "
            >
              Xem chi tiết
            </button>
            <button className=" flex items-center justify-center w-1/2 h-[90%] space-x-2  hover:h-full  bg-purple-700 hover:text-white rounded-lg">
              <ShoppingCartOutlined className="text-xl" /> <span>Mua vé</span>
            </button>
          </div>
        </div>
      </div>
      <img
        className="w-full h-full object-cover rounded"
        src={dataMovie.hinhAnh}
        alt="Sunset in the mountains"
      />
    </div>
  );
}
