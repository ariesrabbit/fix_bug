import { Button, Modal } from "antd";
import React, { useState } from "react";
import { PlayCircleOutlined, PlayCircleFilled } from "@ant-design/icons";
import { linkPressPlayVideo } from "../../../asset/img/linkImg";
export default function ItemMovieVideo({ trailer }) {
  const [open, setOpen] = useState(false);
  return (
    <div>
      <div
        onClick={() => setOpen(true)}
        className="h-14 w-14 hover:h-16 hover:w-16 transition"
      >
        <img width={"100%"} height={"100%"} src={linkPressPlayVideo} alt="" />
      </div>
      {open ? (
        <Modal
          bodyStyle={{ padding: "0px" }}
          footer={false}
          centered
          open={open}
          onOk={() => setOpen(false)}
          onCancel={() => setOpen(false)}
          width={900}
        >
          <iframe
            width={"100%"}
            height={500}
            src={`${trailer}?autoplay=1`}
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          ></iframe>
        </Modal>
      ) : null}
    </div>
  );
}
