import React, { useEffect, useRef, useState } from "react";
import { movieSerVice } from "../../../Service/MovieService";
import { Carousel } from "antd";
import { RightOutlined, LeftOutlined } from "@ant-design/icons";
import styled from "styled-components";
export default function BannerHomePage() {
  let [listBannerMovie, setListBannerMovie] = useState([]);
  let carouselRef = useRef();
  let fetchBanner = async () => {
    try {
      let res = await movieSerVice.getBannerMovie();
      setListBannerMovie(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    fetchBanner();
  }, []);
  let renderBannerListMovie = () => {
    return listBannerMovie.map((bannerMovie) => {
      return (
        <div className="h-[29rem]" key={bannerMovie.maBanner}>
          <img className="w-full h-full" src={bannerMovie.hinhAnh} alt="" />
        </div>
      );
    });
  };
  const CustomCarosel = styled(Carousel)`
    .slick-dots li button {
      height: 9px;
      border-radius: 4px;
    }
  `;
  return (
    <div>
      <CustomCarosel ref={carouselRef} autoplay className="relative">
        {renderBannerListMovie()}
      </CustomCarosel>
      <button
        onClick={() => {
          carouselRef.current.next();
        }}
        className="absolute top-1/4 right-4 text-[5rem] text-slate-100 opacity-30 hover:opacity-60 font-bold duration-300 z-10"
      >
        <RightOutlined />
      </button>
      <button
        onClick={() => {
          carouselRef.current.prev();
        }}
        className="absolute top-1/4 left-4 text-[5rem] text-slate-100 opacity-30 hover:opacity-60 font-bold duration-300 z-10"
      >
        <LeftOutlined />
      </button>
    </div>
  );
}
